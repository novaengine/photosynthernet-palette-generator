import json
from os import listdir, environ
from os.path import isfile, join
from Cloud import Cloud
import sys


import requests

from Palette import Palette
from communication import Communication

def send_update(user_id, payload):
    print("Sending to : {}".format(user_id))
    coms.message(user_id, json.dumps(payload))

def get_palette(payload):
    job = payload.get('job')
    file_name = payload.get('file_path')
    user_id = payload.get('user_id')
    image_id = payload.get('image_id')

    cloud = Cloud()
    cloud.download_file(file_name)

    segments = 1000
    if payload.get('segments'):
        segments = payload.get('segments')

    file_path = join('/tmp/', file_name)

    print(job, file_path)
    palette = Palette(file_path)
    palette.file_name = file_name
    palette.image_id = image_id

    palette.base_colors = colors['data']

    # Analysis Profile Parameters
    palette.segments = segments

    print("Segments : {}".format(palette.segments))

    image_analysis = palette.get_palette(callback=lambda payload : send_update(user_id, payload))

    new_payload = {
        "palette": image_analysis["palette"],
        "meta": image_analysis["meta"],
        "image_id": payload.get('image_id')
    }
    coms.sendJob('photosynthernet_database_test-channel', json.dumps(new_payload))


if __name__ == '__main__':

    colors = []

    colors_req = requests.get('https://photosynther.net/api/colors')

    if colors_req.status_code == 200:
        colors = json.loads(colors_req.content)

    coms = Communication()
    coms.connect(host=environ.get('REDIS_HOST'))
    coms.listen('photosynthernet_database_queues:getimagepalette', method=lambda payload: get_palette(payload))
