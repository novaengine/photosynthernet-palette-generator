import json

import redis


class Communication:
    channel_name = 'default'

    def __init__(self):

        pass

    def connect(self, host):
        self.conn = redis.Redis(host=host, port=6379, db=0)

    def message(self, channel, message):
        self.conn.publish(channel, message)

    def sendJob(self, channel, message):
        self.conn.lpush(channel, message)
        return self

    def listen(self, channel, wait=5, method=lambda payload: payload):
        while True:
            try:
                packed = self.conn.blpop(channel, wait)

                if not packed:
                    continue

                payload = json.loads(packed[1])
                method(payload)

            except KeyboardInterrupt:
                break

    def channel(self, name):
        self.channel_name = name

        return self
